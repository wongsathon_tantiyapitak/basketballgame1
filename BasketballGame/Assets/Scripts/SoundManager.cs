﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private SoundClip[] soundClips;
        
    public static SoundManager Instance { get; private set; }

    private AudioSource audioSource;
    public void Awake()
    {
        Debug.Assert(soundClips != null && soundClips.Length != 0,"Sound clips need to be setup");
        audioSource = GetComponent<AudioSource>();

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }

    }
    private void Start()
    {
        Play(Sound.BGM);
    }
    public enum Sound
    {
        BGM,
        Shoot,
        GetScore
    }
        
    [Serializable]
    public struct SoundClip
    {
        public Sound Sound;
        public AudioClip AudioClip;
        [Range(0,1)] public float SoundVolume;
        public bool loop;
        [HideInInspector]
        public AudioSource audioSource;
    }

    public void Play(Sound sound)
    {
        var soundClip = GetSoundClip(sound);
        if (soundClip.audioSource == null)
        {Debug.Log("null");
            soundClip.audioSource = gameObject.AddComponent<AudioSource>();
        }
        soundClip.audioSource.clip = soundClip.AudioClip;
        soundClip.audioSource.volume = soundClip.SoundVolume;
        soundClip.audioSource.loop = soundClip.loop;
        soundClip.audioSource.Play();
    }
    
    private SoundClip GetSoundClip(Sound sound)
    {
        foreach (var soundClip in soundClips)
        {
            if (soundClip.Sound == sound)
            {
                return soundClip;
            }   
        }

        return default;
    }
}
