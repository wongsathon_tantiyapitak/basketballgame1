using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Npc : MonoBehaviour
{
    [SerializeField] private Rigidbody basketball;
    [SerializeField] public Transform shootPoint;
    [SerializeField] private float speed;
    [SerializeField] private float acceleration;
    public Vector3 npcposition = new Vector3(0,0,10);
    private bool posCheck;


    void Start()
    {            

        basketball.GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        
        if (transform.position.x > 4)
        {
            posCheck = true;
        }
        else if (transform.position.x < -5.5)
        {
            posCheck = false;
        }

        if (posCheck)
        {
            AutoMoveLeft();
        }
        else
        {
            AutoMoveRight();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }
    
    public void Shoot()
    {
        Rigidbody basketball = Instantiate(this.basketball, shootPoint.position, Quaternion.identity);
        basketball.AddForce(0,Force(),Force());
        SoundManager.Instance.Play(SoundManager.Sound.Shoot);
    }

    public float Force()
    {
        float force = acceleration * basketball.mass;
        return force;
    }
    private void AutoMoveRight()
    {
        
        var newXPos = transform.position.x + (speed * Time.deltaTime);

        transform.position = new Vector3(newXPos,transform.position.y,transform.position.z);
    }
    private void AutoMoveLeft()
    {
        
        var newXPos = transform.position.x + (-speed * Time.deltaTime);

        transform.position = new Vector3(newXPos,transform.position.y,transform.position.z);
    }
}
