﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerMovement player;
    [SerializeField] private Npc npc;
    [SerializeField] private float settime;
    [SerializeField] public CinemachineFreeLook Camera;
    [SerializeField] public Transform ring;
    private float time1;
    public bool is1cleared = false;
    private float time2;
    private bool isStart;
    public event Action OnRestarted;
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        time1 = settime;
        time2 = settime;
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void StartGame()
    {
        Camera.LookAt = ring;
        if (is1cleared == false)
        {
            ScoreManager.Instance.Init(this);
            ScoreManager.Instance.reset();
        }
        isStart = true;
        SpawnPlayer();
    }

    void Update()
    {
        if (is1cleared)
        {
            if (isStart)
            {
                time2 -= Time.deltaTime;
                UIManager2.Instance.Settime(time2);
            }
            if (time2 <= 0)
            {
                UIManager2.Instance.End();
                DestroyRemainingObjects();
            }
        }
        else
        {
            if (isStart)
            {
                time1 -= Time.deltaTime;
                UIManager.Instance.Settime(time1);
            }
            if (time1 <= 0)
            {
                is1cleared = true;
                Dialog2();
                DestroyRemainingObjects();
            }
        }
    }
    
    private void Dialog2()
    {
        isStart = false;
        UIManager.Instance.End();
    }

    public void NextScene()
    {
        isStart = true;
        SceneManager.LoadScene(1);
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
        Camera.transform.position = Vector3.zero;
        DestroyRemainingObjects();
        isStart = false;
        UIManager.Instance.dialog1.gameObject.SetActive(true);
        OnRestarted?.Invoke();
    }
    
    private void DestroyRemainingObjects()
    {
        var remainingballs = GameObject.FindGameObjectsWithTag("Ball");
        var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
        var remainingNPC = GameObject.FindGameObjectsWithTag("npc");
        var remainingCamera = GameObject.FindGameObjectsWithTag("Camera");
        foreach (var ball in remainingballs) 
        { 
            Destroy(ball);
        }
        foreach (var player in remainingPlayers) 
        { 
            Destroy(player);
        }
        foreach (var npc in remainingNPC) 
        { 
            Destroy(npc);
        }
        foreach (var camera in remainingCamera) 
        { 
            Destroy(camera);
        }
    }
    
    private void SpawnPlayer()
    {
        var Camera = Instantiate(this.Camera);
        var spawnPlayer = Instantiate(player);
        if (is1cleared)
        {
            var spawnnpc = Instantiate(npc,npc.npcposition,Quaternion.identity);
        }
        Camera.Follow = spawnPlayer.transform;
    }
}
