using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallCheck : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ring")
        {
            ScoreManager.Instance.SetScore();
            ScoreManager.Instance.OnOnScoreUpdated();
        }
        Destroy(gameObject);
    }
}
