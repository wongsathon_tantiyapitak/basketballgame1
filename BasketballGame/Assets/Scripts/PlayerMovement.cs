﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController Controller;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody basketball;
    [SerializeField] public Transform shootPoint;
    [SerializeField] public float DisZ;
    [SerializeField] public float DisY;

    void Start()
    {
        basketball.GetComponent<Rigidbody>();

    }
    
    void FixedUpdate()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal,0f,vertical).normalized;
        
        if (direction.magnitude >= 0.1f)
        {
            Controller.Move(direction * speed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        
    }
    
    public void Shoot()
    {
        Vector3 V_originXY = CalculateVelocity(shootPoint.position, shootPoint.position, 1f);
        Rigidbody basketball = Instantiate(this.basketball, shootPoint.position, Quaternion.identity);
        SoundManager.Instance.Play(SoundManager.Sound.Shoot);
        basketball.velocity = V_originXY;
        
    }
    Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        target.z += DisZ;
        target.y += DisY;
        Vector3 distance = target - origin;
        Vector3 distanceXZ_plane = distance;
        distanceXZ_plane.y = 0f;

        float distanceXZ = distanceXZ_plane.magnitude;
        float distanceY = distance.y;
        
        float Vxz = distanceXZ / time;
        float Vy = distanceY / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXZ_plane.normalized;
        result *= Vxz;
        result.y = Vy;
        return result;
    }
}
