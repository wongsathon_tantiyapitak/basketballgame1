using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button startButton;
    [SerializeField] private Button startButton2;
    [SerializeField] public RectTransform dialog1;
    [SerializeField] public RectTransform dialog2;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI finalScoreText;
    [SerializeField] private TextMeshProUGUI remainingTime;
    public static UIManager Instance { get; private set; }
    
    private void OnStartButtonClicked()
    {
        dialog1.gameObject.SetActive(false);
        dialog2.gameObject.SetActive(false);
        GameManager.Instance.StartGame();
    }

    private void OnStartButtonClicked2()
    {
        ScoreManager.Instance.reset();
        GameManager.Instance.NextScene();
    }
    private void HideScore(bool hide)
    {
        scoreText.gameObject.SetActive(!hide);
    }
    private void Awake()
    {
        startButton.onClick.AddListener(OnStartButtonClicked);
        startButton2.onClick.AddListener(OnStartButtonClicked2);
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        GameManager.Instance.OnRestarted += RestartUI;
        ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
    }

    private void OnRestarted()
    {
        GameManager.Instance.OnRestarted -= OnRestarted;
        HideScore(true);
        ScoreManager.Instance.reset();
    }

    public void Settime(float time)
    {
        remainingTime.text = $"Time : {time:0}";
    }
    public void End()
    {
        finalScoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
        dialog2.gameObject.SetActive(true);
        finalScoreText.gameObject.SetActive(true);
    }
    private void RestartUI()
    {
        
    }

    private void UpdateScoreUI()
    {
        scoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
    }
}
