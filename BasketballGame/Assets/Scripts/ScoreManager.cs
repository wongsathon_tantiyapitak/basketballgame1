﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    
    public event Action OnScoreUpdated;
    public static ScoreManager Instance { get; private set; }

    private GameManager gameManager;
    private int playerscore;

    public void Init(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    public void reset()
    {
        playerscore = 0;
    }
    public void SetScore()
    {
        playerscore++;
    }

    public int GetScore()
    {
        return playerscore;
    }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void OnOnScoreUpdated()
    {
        OnScoreUpdated?.Invoke();
    }
}
