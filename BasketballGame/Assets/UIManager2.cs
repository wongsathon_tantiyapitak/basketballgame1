using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager2 : MonoBehaviour
{
    [SerializeField] private Button restartButton;
    [SerializeField] public RectTransform dialog1;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI finalScoreText;
    [SerializeField] private TextMeshProUGUI remainingTime;
    public static UIManager2 Instance;
    
    private void OnRestartButtonClicked()
    {
        GameManager.Instance.is1cleared = false;
        dialog1.gameObject.SetActive(false);
        GameManager.Instance.Restart();
    }

    private void HideScore(bool hide)
    {
        scoreText.gameObject.SetActive(!hide);
    }
    private void Awake()
    {
        restartButton.onClick.AddListener(OnRestartButtonClicked);
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        dialog1.gameObject.SetActive(false);
        GameManager.Instance.OnRestarted += RestartUI;
        ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
    }

    private void OnRestarted()
    {
        GameManager.Instance.OnRestarted -= OnRestarted;
        HideScore(true);
        ScoreManager.Instance.reset();
    }

    public void End()
    {
        finalScoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
        dialog1.gameObject.SetActive(true);
        finalScoreText.gameObject.SetActive(true);
    }
    public void Settime(float time)
    {
        remainingTime.text = $"Time : {time:0}";
    }
    private void RestartUI()
    {
        
    }

    private void UpdateScoreUI()
    {
        scoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
    }
}
